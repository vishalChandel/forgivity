package com.forgivity.app.util

// - - Base Server Url
const val BASE_URL = "https://www.dharmani.com/Forgivity/Api/"

// - - Constant Values
const val SPLASH_TIME_OUT = 1500L
const val ACCEPT = 1
const val REJECT = 2

// - - Link Constants
const val LINK_TYPE = "link_type"
const val LINK_ABOUT = "link_about"
const val LINK_PP = "link_pp"
const val LINK_TERMS = "link_terms"

// - - Link Urls
const val ABOUT_WEB_LINK = BASE_URL + "webservice/about.html"
const val PP_WEB_LINK = "https://www.forgivity.com/privacy"
const val TERMS_WEB_LINK ="https://www.forgivity.com/terms"

// - - Shared Preference Keys
const val ACCESS_TOKEN = "access_token"
const val IS_LOGIN = "is_login"
var NAME = "name"
var AGE = "age"
var GENDER = "gender"
var INTEREST = "interest"
var PERSON_TYPE = "person_type"
var PROFILE_IMAGE = "profileImage"
const val PHONE_NUMBER = "phone_number"
