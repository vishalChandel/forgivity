package com.forgivity.app.viewModel

import android.app.Activity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.forgivity.app.model.GlossaryModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.views.activity.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GlossaryViewModel : ViewModel() {
    private var mModel: MutableLiveData<GlossaryModel>? = null

    fun glossaryData(
        activity: Activity?,
        access_token:String,
        list_type:String,
        page_no:String,
        search:String
    ): MutableLiveData<GlossaryModel>? {
        mModel = MutableLiveData<GlossaryModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.glossaryRequest(access_token,list_type,page_no,search).enqueue(object : Callback<GlossaryModel?> {
            override fun onResponse(
                call: Call<GlossaryModel?>?,
                response: Response<GlossaryModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<GlossaryModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mModel
    }
}