package com.forgivity.app.viewModel

import android.app.Activity
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.forgivity.app.model.GetProfileModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.views.activity.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GetProfileViewModel : ViewModel() {
    private var mModel: MutableLiveData<GetProfileModel>? = null

    fun getProfileData(
        activity: Activity?,
        access_token:String
    ): MutableLiveData<GetProfileModel>? {
        mModel = MutableLiveData<GetProfileModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getProfileRequest(access_token).enqueue(object : Callback<GetProfileModel?> {
            override fun onResponse(
                call: Call<GetProfileModel?>?,
                response: Response<GetProfileModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<GetProfileModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mModel
    }
}