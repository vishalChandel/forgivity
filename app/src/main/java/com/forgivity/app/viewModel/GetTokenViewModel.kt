package com.forgivity.app.viewModel

import android.app.Activity
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.forgivity.app.model.GetTokenModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.views.activity.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GetTokenViewModel : ViewModel() {
    private var mModel: MutableLiveData<GetTokenModel>? = null

    fun getTokenData(
        activity: Activity?,
        org_id:String
    ): MutableLiveData<GetTokenModel>? {
        mModel = MutableLiveData<GetTokenModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getTokenRequest(org_id).enqueue(object : Callback<GetTokenModel?> {
            override fun onResponse(
                call: Call<GetTokenModel?>?,
                response: Response<GetTokenModel?>
            ) {
                if (response.body() != null) {
                    mModel!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<GetTokenModel?>?, t: Throwable?) {
            }
        })
        return mModel
    }
}