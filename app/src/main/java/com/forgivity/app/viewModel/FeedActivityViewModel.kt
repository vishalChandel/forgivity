package com.forgivity.app.viewModel

import android.app.Activity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.forgivity.app.model.FeedActivityModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.views.activity.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FeedActivityViewModel : ViewModel() {
    private var mModel: MutableLiveData<FeedActivityModel>? = null

    fun activityListData(
        activity: Activity?,
        access_token:String,
        page_no:String
    ): MutableLiveData<FeedActivityModel>? {
        mModel = MutableLiveData<FeedActivityModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getActivityListRequest(access_token,page_no).enqueue(object : Callback<FeedActivityModel?> {
            override fun onResponse(
                call: Call<FeedActivityModel?>?,
                response: Response<FeedActivityModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<FeedActivityModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mModel
    }
}