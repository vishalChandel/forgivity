package com.forgivity.app.viewModel

import android.app.Activity
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.forgivity.app.model.GetTokenModel
import com.forgivity.app.model.UpdateProfileModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.views.activity.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UpdateProfileViewModel : ViewModel() {
    private var mModel: MutableLiveData<UpdateProfileModel>? = null

    fun updateProfileData(
        activity: Activity?,
        org_id: String,
        name: String,
        age: String,
        interest: String,
        gender: String,
        person_type: String,
        access_token: String,
        device_token: String,
        device_type: String
    ): MutableLiveData<UpdateProfileModel>? {
        mModel = MutableLiveData<UpdateProfileModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.updateProfileRequest(
            org_id,
            name,
            age,
            interest,
            gender,
            person_type,
            access_token,
            device_token,
            device_type
        ).enqueue(object : Callback<UpdateProfileModel?> {
            override fun onResponse(
                call: Call<UpdateProfileModel?>?,
                response: Response<UpdateProfileModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<UpdateProfileModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mModel
    }
}