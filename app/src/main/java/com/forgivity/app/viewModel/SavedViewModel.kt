package com.forgivity.app.viewModel

import android.app.Activity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.forgivity.app.model.SavedModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.views.activity.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SavedViewModel : ViewModel() {
    private var mModel: MutableLiveData<SavedModel>? = null

    fun savedListData(
        activity: Activity?,
        access_token:String,
        page_no:String
    ): MutableLiveData<SavedModel>? {
        mModel = MutableLiveData<SavedModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getSavedListRequest(access_token,page_no).enqueue(object : Callback<SavedModel?> {
            override fun onResponse(
                call: Call<SavedModel?>?,
                response: Response<SavedModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<SavedModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mModel
    }
}