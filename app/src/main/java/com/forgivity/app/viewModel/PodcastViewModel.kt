package com.forgivity.app.viewModel

import android.app.Activity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.forgivity.app.model.PodcastModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.views.activity.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PodcastViewModel : ViewModel() {
    private var mModel: MutableLiveData<PodcastModel>? = null

    fun podcastData(
        activity: Activity?,
        access_token:String,
        list_type:String,
        page_no:String,
        search:String
    ): MutableLiveData<PodcastModel>? {
        mModel = MutableLiveData<PodcastModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.podcastRequest(access_token,list_type,page_no,search).enqueue(object : Callback<PodcastModel?> {
            override fun onResponse(
                call: Call<PodcastModel?>?,
                response: Response<PodcastModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<PodcastModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mModel
    }
}