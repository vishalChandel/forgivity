package com.forgivity.app.viewModel

import android.app.Activity
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.forgivity.app.model.GlossaryDetailModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.views.activity.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GlossaryDetailViewModel : ViewModel() {
    private var mModel: MutableLiveData<GlossaryDetailModel>? = null

    fun glossaryDetailData(
        activity: Activity?,
        access_token:String,
        glos_id:String
    ): MutableLiveData<GlossaryDetailModel>? {
        mModel = MutableLiveData<GlossaryDetailModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.glossaryDetailRequest(access_token,glos_id).enqueue(object : Callback<GlossaryDetailModel?> {
            override fun onResponse(
                call: Call<GlossaryDetailModel?>?,
                response: Response<GlossaryDetailModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<GlossaryDetailModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mModel
    }
}