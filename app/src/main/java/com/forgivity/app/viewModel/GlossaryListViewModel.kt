package com.forgivity.app.viewModel

import android.app.Activity
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.views.activity.BaseActivity
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GlossaryListViewModel : ViewModel() {
    private var mModel: MutableLiveData<JsonObject>? = null

    fun getGlossaryListData(
        activity: Activity?,
        access_token:String
    ): MutableLiveData<JsonObject>? {
        mModel = MutableLiveData<JsonObject>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getGlossaryListRequest(access_token).enqueue(object : Callback<JsonObject?> {
            override fun onResponse(
                call: Call<JsonObject?>?,
                response: Response<JsonObject?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<JsonObject?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mModel
    }
}