package com.forgivity.app.viewModel

import android.app.Activity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.forgivity.app.model.DayModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.views.activity.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DayViewModel : ViewModel() {
    private var mModel: MutableLiveData<DayModel>? = null

    fun dayData(
        activity: Activity?,
        access_token:String,
        list_type:String,
        page_no:String,
        search:String
    ): MutableLiveData<DayModel>? {
        mModel = MutableLiveData<DayModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.dayRequest(access_token,list_type,page_no,search).enqueue(object : Callback<DayModel?> {
            override fun onResponse(
                call: Call<DayModel?>?,
                response: Response<DayModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<DayModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mModel
    }
}