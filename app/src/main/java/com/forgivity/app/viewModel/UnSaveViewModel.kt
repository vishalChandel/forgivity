package com.forgivity.app.viewModel

import android.app.Activity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.forgivity.app.model.UnSaveModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.views.activity.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UnSaveViewModel : ViewModel() {
    private var mModel: MutableLiveData<UnSaveModel>? = null

    fun unSaveData(
        activity: Activity?,
        access_token:String,
        type_id:String,
        type:String
    ): MutableLiveData<UnSaveModel>? {
        mModel = MutableLiveData<UnSaveModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.unSaveDataRequest(access_token,type_id,type).enqueue(object : Callback<UnSaveModel?> {
            override fun onResponse(
                call: Call<UnSaveModel?>?,
                response: Response<UnSaveModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<UnSaveModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mModel
    }
}