package com.forgivity.app.model

data class RelatedPod(
    val pod_creation: String,
    val pod_desc: String,
    val pod_disable: String,
    val pod_display_title: String,
    val pod_file: String,
    val pod_id: String,
    val pod_title: String,
    val pod_type: String,
    val welcome_pod: String
)