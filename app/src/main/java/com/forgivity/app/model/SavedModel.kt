package com.forgivity.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SavedModel(

	@field:SerializedName("all_data")
	val allData: AllDataSaved? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class ListItem(

	@field:SerializedName("saved_type")
	val savedType: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("saved_id")
	val savedId: String? = null,

	@field:SerializedName("desc")
	val desc: String? = null
) : Parcelable

@Parcelize
data class AllDataSaved(

	@field:SerializedName("last_page")
	val lastPage: Boolean? = null,

	@field:SerializedName("list")
	val list: ArrayList<ListItem?>? = null
) : Parcelable
