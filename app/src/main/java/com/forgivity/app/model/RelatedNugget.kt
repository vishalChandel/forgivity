package com.forgivity.app.model

data class RelatedNugget(
    val nug_creation: String,
    val nug_desc: String,
    val nug_disable: String,
    val nug_id: String,
    val nug_save_status: Boolean,
    val nug_title: String,
    val read_status: Int,
    val related_audio: String,
    val related_pod: RelatedPod,
    val related_ques: String,
    val related_quiz: String
)