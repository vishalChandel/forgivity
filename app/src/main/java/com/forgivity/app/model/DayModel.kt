package com.forgivity.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DayModel(

	@field:SerializedName("all_data")
	val allData: AllDataDay? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class ListItemDay(

	@field:SerializedName("day_desc")
	val dayDesc: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("day_disable")
	val dayDisable: String? = null,

	@field:SerializedName("related_nuggets")
	val relatedNuggets: String? = null,

	@field:SerializedName("day_creation")
	val dayCreation: String? = null,

	@field:SerializedName("day_level")
	val dayLevel: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("pod_title")
	val podTitle: String? = null,

	@field:SerializedName("day_id")
	val dayId: String? = null,

	@field:SerializedName("related_pod")
	val relatedPod: String? = null
) : Parcelable

@Parcelize
data class AllDataDay(

	@field:SerializedName("last_page")
	val lastPage: Boolean? = null,

	@field:SerializedName("list")
	val list: ArrayList<ListItemDay?>? = null,

	@field:SerializedName("type_file")
	val typeFile: String? = null
) : Parcelable
