package com.forgivity.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PodcastModel(

	@field:SerializedName("all_data")
	val allData: AllData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class ListItemPodcast(

	@field:SerializedName("pod_file")
	val podFile: String? = null,

	@field:SerializedName("pod_display_title")
	val podDisplayTitle: String? = null,

	@field:SerializedName("pod_creation")
	val podCreation: String? = null,

	@field:SerializedName("pod_disable")
	val podDisable: String? = null,

	@field:SerializedName("welcome_pod")
	val welcomePod: String? = null,

	@field:SerializedName("pod_type")
	val podType: String? = null,

	@field:SerializedName("pod_id")
	val podId: String? = null,

	@field:SerializedName("pod_title")
	val podTitle: String? = null,

	@field:SerializedName("pod_desc")
	val podDesc: String? = null
) : Parcelable

@Parcelize
data class AllData(

	@field:SerializedName("last_page")
	val lastPage: Boolean? = null,

	@field:SerializedName("list")
	val list: ArrayList<ListItemPodcast?>? = null,

	@field:SerializedName("type_file")
	val typeFile: String? = null
) : Parcelable
