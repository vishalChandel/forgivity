package com.forgivity.app.model

data class DayDetail(
    val day_creation: String,
    val day_desc: String,
    val day_desc_read: Int,
    val day_disable: String,
    val day_id: String,
    val day_level: String,
    val day_save_status: Boolean,
    val related_nuggets: List<RelatedNugget>,
    val related_pod: RelatedPodX
)