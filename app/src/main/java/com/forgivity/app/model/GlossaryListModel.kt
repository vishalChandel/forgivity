package com.forgivity.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GlossaryListModel(

	@field:SerializedName("disable")
	var disable: String? = null,

	@field:SerializedName("glossary_id")
	var glossaryId: String? = null,

	@field:SerializedName("definition")
	var definition: String? = null,

	@field:SerializedName("creation_date")
	var creationDate: String? = null,

	@field:SerializedName("word")
	var word: String? = null,

	@field:SerializedName("page_refs")
	var pageRefs: String? = null
) : Parcelable
