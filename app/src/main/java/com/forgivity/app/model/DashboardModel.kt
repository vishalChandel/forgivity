package com.forgivity.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DashboardModel(

	@field:SerializedName("user_detail")
	val userDetail: UserDetail1? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class UserDetail1(

	@field:SerializedName("user_email")
	val userEmail: String? = null,

	@field:SerializedName("user_name")
	val userName: String? = null,

	@field:SerializedName("person_type")
	val personType: String? = null,

	@field:SerializedName("notification_disable")
	val notificationDisable: String? = null,

	@field:SerializedName("check_in")
	val checkIn: Int? = null,

	@field:SerializedName("user_age")
	val userAge: String? = null,

	@field:SerializedName("device_type")
	val deviceType: String? = null,

	@field:SerializedName("user_gender")
	val userGender: String? = null,

	@field:SerializedName("time_spent")
	val timeSpent: Int? = null,

	@field:SerializedName("access_token")
	val accessToken: String? = null,

	@field:SerializedName("day_completed")
	val dayCompleted: Int? = null,

	@field:SerializedName("user_type")
	val userType: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("saved_days")
	val savedDays: Int? = null,

	@field:SerializedName("org_id")
	val orgId: String? = null,

	@field:SerializedName("device_token")
	val deviceToken: String? = null,

	@field:SerializedName("user_creation")
	val userCreation: String? = null,

	@field:SerializedName("saved_tools")
	val savedTools: Int? = null,

	@field:SerializedName("user_disable")
	val userDisable: String? = null,

	@field:SerializedName("level_completed")
	val levelCompleted: Int? = null,

	@field:SerializedName("user_interest")
	val userInterest: String? = null,

	@field:SerializedName("user_pass")
	val userPass: String? = null
) : Parcelable
