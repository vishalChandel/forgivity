package com.forgivity.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GlossaryModel(

	@field:SerializedName("all_data")
	val allData: AllDataGlossary? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class AllDataGlossary(

	@field:SerializedName("last_page")
	val lastPage: Boolean? = null,

	@field:SerializedName("list")
	val list: ArrayList<ListItemGlossary?>? = null,

	@field:SerializedName("type_file")
	val typeFile: String? = null
) : Parcelable

@Parcelize
data class ListItemGlossary(

	@field:SerializedName("disable")
	val disable: String? = null,

	@field:SerializedName("glossary_id")
	val glossaryId: String? = null,

	@field:SerializedName("definition")
	val definition: String? = null,

	@field:SerializedName("creation_date")
	val creationDate: String? = null,

	@field:SerializedName("word")
	val word: String? = null,

	@field:SerializedName("page_refs")
	val pageRefs: String? = null
) : Parcelable
