package com.forgivity.app.model

data class GetQuestionsModel(
    val day_detail: DayDetail,
    val end_date: String,
    val message: String,
    val status: Int
)