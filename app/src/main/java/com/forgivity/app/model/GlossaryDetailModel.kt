package com.forgivity.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GlossaryDetailModel(

	@field:SerializedName("saved_detail")
	val savedDetail: SavedDetail? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class SavedDetail(

	@field:SerializedName("disable")
	val disable: String? = null,

	@field:SerializedName("glossary_id")
	val glossaryId: String? = null,

	@field:SerializedName("definition")
	val definition: String? = null,

	@field:SerializedName("creation_date")
	val creationDate: String? = null,

	@field:SerializedName("linked_pages")
	val linkedPages: List<LinkedPagesItem?>? = null,

	@field:SerializedName("word")
	val word: String? = null,

	@field:SerializedName("page_refs")
	val pageRefs: String? = null
) : Parcelable

@Parcelize
data class LinkedPagesItem(

	@field:SerializedName("read_status")
	val readStatus: Int? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("nug_id")
	val nugId: String? = null,

	@field:SerializedName("title")
	val title: String? = null
) : Parcelable
