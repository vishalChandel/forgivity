package com.forgivity.app.retrofit

import com.forgivity.app.model.*
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ApiInterface {

    @FormUrlEncoded
    @POST("get_token")
    fun getTokenRequest(
        @Field("org_id") org_id: String
    ): Call<GetTokenModel>

    @FormUrlEncoded
    @POST("update_profile")
    fun updateProfileRequest(
        @Field("org_id") org_id: String,
        @Field("name") name: String,
        @Field("age") age: String,
        @Field("interest") interest: String,
        @Field("gender") gender: String,
        @Field("person_type") person_type: String,
        @Field("access_token") access_token: String,
        @Field("device_token") device_token: String,
        @Field("device_type") device_type: String
    ): Call<UpdateProfileModel>

    @FormUrlEncoded
    @POST("get_profile")
    fun getProfileRequest(
        @Field("access_token") access_token: String
    ): Call<GetProfileModel>

    @FormUrlEncoded
    @POST("dashboard")
    fun dashboardRequest(
        @Field("access_token") access_token: String
    ): Call<DashboardModel>

    @FormUrlEncoded
    @POST("get_glossary_list")
    fun getGlossaryListRequest(
        @Field("access_token") access_token: String
    ): Call<JsonObject>

    @FormUrlEncoded
    @POST("get_glossary_details")
    fun glossaryDetailRequest(
        @Field("access_token") access_token: String,
        @Field("glos_id") glos_id: String
    ): Call<GlossaryDetailModel>

    @FormUrlEncoded
    @POST("get_activity_list")
    fun getActivityListRequest(
        @Field("access_token") access_token: String,
        @Field("page_no") page_no: String
    ): Call<FeedActivityModel>

    @FormUrlEncoded
    @POST("get_list")
    fun glossaryRequest(
        @Field("access_token") access_token: String,
        @Field("list_type") list_type: String,
        @Field("page_no") page_no: String,
        @Field("search") search: String
    ): Call<GlossaryModel>

    @FormUrlEncoded
    @POST("get_list")
    fun dayRequest(
        @Field("access_token") access_token: String,
        @Field("list_type") list_type: String,
        @Field("page_no") page_no: String,
        @Field("search") search: String
    ): Call<DayModel>

    @FormUrlEncoded
    @POST("get_list")
    fun podcastRequest(
        @Field("access_token") access_token: String,
        @Field("list_type") list_type: String,
        @Field("page_no") page_no: String,
        @Field("search") search: String
    ): Call<PodcastModel>

    @FormUrlEncoded
    @POST("get_saved_list")
    fun getSavedListRequest(
        @Field("access_token") access_token: String,
        @Field("page_no") page_no: String
    ): Call<SavedModel>

    @FormUrlEncoded
    @POST("unsave_data")
    fun unSaveDataRequest(
        @Field("access_token") access_token: String,
        @Field("type_id") page_no: String,
        @Field("type") type: String
    ): Call<UnSaveModel>

    @FormUrlEncoded
    @POST("save_data")
    fun saveDataRequest(
        @Field("access_token") access_token: String,
        @Field("type_id") page_no: String,
        @Field("type") type: String
    ): Call<SaveModel>


    @FormUrlEncoded
    @POST("get_reading")
    fun getQuestionRequest(
        @Field("access_token") access_token: String
    ): Call<SaveModel>

}


