package com.forgivity.app.views.activity

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.os.SystemClock
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.forgivity.app.R
import com.forgivity.app.util.*
import java.util.*


open class BaseActivity : AppCompatActivity() {

    // - - Get Class Name
    var TAG = this@BaseActivity.javaClass.simpleName

    // - - Initialize Activity
    var mActivity: Activity = this@BaseActivity

    // - - Initialize other class objects
    var progressDialog: Dialog? = null
    private var mLastClickTab1: Long = 0
    // - - To Get UserID
    fun getAccessToken(): String {
        return AppPreference().readString(mActivity, ACCESS_TOKEN, "")!!
    }

    // - - To Get Auth_Token
    fun getName(): String {
        return AppPreference().readString(mActivity, NAME, "")!!
    }

    // - - To Get Email
    fun getAge(): String {
        return AppPreference().readString(mActivity, AGE, "")!!
    }

    // - - To Get Bio
    fun getGender(): String {
        return AppPreference().readString(mActivity, GENDER, "")!!
    }

    // - - To Get Bio
    fun getInterest(): String {
        return AppPreference().readString(mActivity, INTEREST, "")!!
    }

    // - - To Get Bio
    fun getPersonType(): String {
        return AppPreference().readString(mActivity, PERSON_TYPE, "")!!
    }

    // - - To Get Bio
    fun getProfileImage(): String {
        return AppPreference().readString(mActivity, PROFILE_IMAGE, "")!!
    }

    //  Check whether user is logged in or not
    fun isLogin(): Boolean {
        return AppPreference().readBoolean(mActivity, IS_LOGIN, false)
    }
    /*
Clear editText focus
 */
    fun setEditTextFocused(mEditText: EditText) {
        mEditText.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEditText.clearFocus()
                val imm = v.context
                    .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
                return@OnEditorActionListener true
            }
            false
        })
    }

    /*
     *
     * Error Alert Dialog
     * */
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    /*
     *
     * Correct Alert Dialog
     * */
    fun showCorrectAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_correct)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDoneCorrect = alertDialog.findViewById<TextView>(R.id.btnDoneCorrect)
        txtMessageTV.text = strMessage
        btnDoneCorrect.setOnClickListener {
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    /*
     *
     * Incorrect Alert Dialog
     * */
    fun showIncorrectAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_incorrect)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDoneIncorrect = alertDialog.findViewById<TextView>(R.id.btnDoneIncorrect)
        txtMessageTV.text = strMessage
        btnDoneIncorrect.setOnClickListener {
            alertDialog.dismiss()
        }

        alertDialog.show()
    }

    /*
 *
 * Correct Alert Dialog
 * */
    fun showSurveyAlertDialog(mActivity: Activity?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_survey_three)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val btnDoneCorrect = alertDialog.findViewById<TextView>(R.id.btnDoneCorrect)
        val imgCrossIV = alertDialog.findViewById<ImageView>(R.id.imgCrossIV)
        btnDoneCorrect.setOnClickListener {
            alertDialog.dismiss()
            showSurveyResultAlertDialog(mActivity)

        }
        imgCrossIV.setOnClickListener {
            alertDialog.dismiss()
//            showSurveyResultAlertDialog(mActivity)
        }
        alertDialog.show()
    }

    /*
*
* Correct Alert Dialog
* */
    private fun showSurveyResultAlertDialog(mActivity: Activity?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_survey_three_result)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val btnDone = alertDialog.findViewById<TextView>(R.id.btnDone)
        btnDone.setOnClickListener {
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    /*
     * Finish the activity
     * */
    override fun finish() {
        super.finish()
//        overridePendingTransitionSlideDownExit()
    }

    /*
     * To Start the New Activity
     * */
    override fun startActivity(intent: Intent?) {
        super.startActivity(intent)
//        overridePendingTransitionSlideUPEnter()
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    fun overridePendingTransitionSlideUPEnter() {
        overridePendingTransition(R.anim.bottom_up, 0)
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    fun overridePendingTransitionSlideDownExit() {
        overridePendingTransition(0, R.anim.bottom_down)
    }

    // - - To Show Progress Dialog
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }

    // - - To Hide Progress Dialog
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

    //  Show Toast Message
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }

    //  To Check Internet Connections
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    open fun preventMultipleClick() {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 2000) {
            return
        }
        mLastClickTab1 = SystemClock.elapsedRealtime()
    }
}