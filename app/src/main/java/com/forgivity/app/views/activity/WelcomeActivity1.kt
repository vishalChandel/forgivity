package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R

class WelcomeActivity1 : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome1)
        ButterKnife.bind(this)
    }

    @OnClick(
        R.id.btnGotItWelcome1,
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.btnGotItWelcome1 -> performGotItClick()
        }
    }

    private fun performGotItClick() {
        startActivity(Intent(mActivity, WelcomeActivity2::class.java))
    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}