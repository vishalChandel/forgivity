package com.forgivity.app.views.activity


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.model.GetTokenModel
import com.forgivity.app.util.ACCESS_TOKEN
import com.forgivity.app.util.AppPreference
import com.forgivity.app.util.IS_LOGIN
import com.forgivity.app.util.NAME
import com.forgivity.app.viewModel.GetTokenViewModel
import kotlinx.android.synthetic.main.activity_name.*

class NameActivity : BaseActivity() {
    // - - Initialize Objects

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_name)
        ButterKnife.bind(this)
        setEditTextFocused(editNameET)
    }

    @OnClick(
        R.id.btnNextName
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.btnNextName -> performNextClick()
        }
    }

    private fun performNextClick() {
        if (isValidate()) {
            AppPreference().writeString(this, NAME, editNameET.text.toString())
            val intent = Intent(mActivity, BirthdayActivity::class.java)
            startActivity(intent)
        }
    }

    // - - Validations
    private fun isValidate(): Boolean {
        var flag = true
        if (editNameET!!.text.toString().trim() == "") {
            showAlertDialog(mActivity, "Please enter your name to continue.")
            flag = false
        }
        return flag
    }


}