package com.forgivity.app.views.activity

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import com.forgivity.app.R
import com.forgivity.app.adapter.GlossaryNuggetsAdapter
import com.forgivity.app.model.GlossaryDetailModel
import com.forgivity.app.model.SavedDetail
import com.forgivity.app.viewModel.GlossaryDetailViewModel
import kotlinx.android.synthetic.main.activity_glossary_detail.*
import java.util.*

class GlossaryDetailActivity : BaseActivity() {

    // - - Initialize Objects
    private var mGlossaryID: String = ""
    private var glossaryDetailViewModel: GlossaryDetailViewModel? = null
    var glossaryNuggetsAdapter: GlossaryNuggetsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_glossary_detail)
        ButterKnife.bind(this)
        glossaryDetailViewModel =
            ViewModelProviders.of(this).get(GlossaryDetailViewModel::class.java)
        getIntentData()
        haulerView.setOnDragDismissedListener {
            // finish activity when dismissed
            finish()
        }

    }

    private fun getIntentData() {
        if (intent != null) {
            mGlossaryID = mGlossaryID.let { intent.getStringExtra("glossary_id").toString() }
            executeGlossaryDetailApi()
        }
    }

    // - - Execute DashBoard Api
    private fun executeGlossaryDetailApi() {
        showProgressDialog(mActivity)
        glossaryDetailViewModel?.glossaryDetailData(mActivity, getAccessToken(), mGlossaryID)
            ?.observe(this,
                androidx.lifecycle.Observer<GlossaryDetailModel?> { mModel ->
                    if (mModel.status == 1) {
                        dismissProgressDialog()
                        setDataOnWidgets(mModel)
                    } else {
                        showAlertDialog(mActivity, mModel.message)
                        dismissProgressDialog()
                    }
                })
    }

    private fun setDataOnWidgets(mModel: GlossaryDetailModel) {
        txtWordAdjectiveTV.visibility = View.VISIBLE
        txtWordTV.text = mModel.savedDetail?.word
        txtWordDetailTV.text = ":"+" " +mModel.savedDetail?.definition
        if (mModel.savedDetail?.linkedPages.isNullOrEmpty()) {
            txtRelatedNuggetTV.visibility = View.GONE
        } else {
            txtRelatedNuggetTV.visibility = View.VISIBLE
            setAdapter(mModel)
        }
    }

    private fun setAdapter(mModel: GlossaryDetailModel) {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        nuggetsListRV.layoutManager = layoutManager
        glossaryNuggetsAdapter = GlossaryNuggetsAdapter(this, mModel)
        nuggetsListRV.adapter = glossaryNuggetsAdapter
    }

}