package com.forgivity.app.views.activity

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.adapter.FeedAdapter
import com.forgivity.app.interfaces.LoadMoreActivityFeedData
import com.forgivity.app.model.FeedActivityModel
import com.forgivity.app.model.ListItemFeed
import com.forgivity.app.viewModel.FeedActivityViewModel
import kotlinx.android.synthetic.main.activity_feed.*

class FeedActivity : BaseActivity(), LoadMoreActivityFeedData {

    // - - Initialize Objects
    var mLoadMore: LoadMoreActivityFeedData? = null
    var mPageNo: Int = 1
    var isLoading: Boolean = true
    var mActivityFeedList: ArrayList<ListItemFeed?>? = ArrayList()
    private var feedActivityViewModel: FeedActivityViewModel? = null
    private var feedAdapter: FeedAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed)
        ButterKnife.bind(this)
        feedActivityViewModel = ViewModelProviders.of(this).get(FeedActivityViewModel::class.java)
        mLoadMore = this
        if (mActivityFeedList != null) {
            mActivityFeedList!!.clear()
        }
        getActivityFeedData()
    }

    @OnClick(
        R.id.imgBackIV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
        }
    }

    private fun getActivityFeedData() {
        if (isNetworkAvailable(mActivity)) {
            executeFeedActivityListApi()
        } else {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        }
    }

    // - - Execute Activity List Api
    private fun executeFeedActivityListApi() {
        showProgressDialog(mActivity)
        feedActivityViewModel?.activityListData(mActivity, getAccessToken(), mPageNo.toString())
            ?.observe(this,
                androidx.lifecycle.Observer<FeedActivityModel?> { mModel ->
                    when (mModel.status) {
                        1 -> {
                            dismissProgressDialog()
                            isLoading = !mModel.allData?.lastPage?.equals(true)!!
                            mActivityFeedList = mModel!!.allData?.list
                            setAdapter()
                            noMoreDataTV.visibility = View.GONE
                        }
                        else -> {
                            showAlertDialog(mActivity, mModel.message)
                            dismissProgressDialog()
                            noMoreDataTV.visibility = View.VISIBLE
                        }
                    }
                })
    }

    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        activityFeedRV.layoutManager = layoutManager
        feedAdapter = FeedAdapter(mActivity, mActivityFeedList, mLoadMore)
        activityFeedRV.adapter = feedAdapter
    }

    private fun loadMoreActivityFeedData() {
        mProgressRL.visibility = View.VISIBLE
        feedActivityViewModel?.activityListData(mActivity, getAccessToken(), mPageNo.toString())
            ?.observe(this, androidx.lifecycle.Observer<FeedActivityModel?> { mModel ->

                if (mModel.status == 1) {
                    mProgressRL.visibility = View.GONE
                    mModel.allData?.list?.let { mActivityFeedList?.addAll(it) }
                    feedAdapter?.notifyDataSetChanged()
                    isLoading = !mModel.allData?.lastPage?.equals(true)!!
                } else if (mModel.status == 0) {
                    mProgressRL.visibility = View.GONE
                }
            })
    }


    override fun onLoadMoreActivityFeedData(mModel: ListItemFeed) {
        if (isLoading) {
            ++mPageNo
            loadMoreActivityFeedData()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right)
    }
}