package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.model.GetProfileModel
import com.forgivity.app.model.UpdateProfileModel
import com.forgivity.app.util.AppPreference
import com.forgivity.app.util.NAME
import com.forgivity.app.util.PERSON_TYPE
import com.forgivity.app.viewModel.GetProfileViewModel
import com.forgivity.app.viewModel.UpdateProfileViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : BaseActivity() {

    // - - Initialize Objects
    var mGender: String? = ""
    var mInterest: String? = ""
    private var getProfileViewModel: GetProfileViewModel? = null
    private var updateProfileViewModel: UpdateProfileViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        ButterKnife.bind(this)
        getProfileViewModel = ViewModelProviders.of(this).get(GetProfileViewModel::class.java)
        updateProfileViewModel = ViewModelProviders.of(this).get(UpdateProfileViewModel::class.java)
        executeGetProfileApi()
        mInterest = getInterest()
        mGender = getGender()
        setEditTextFocused(editAgeEt)
    }

    @OnClick(
        R.id.txtInterestTV,
        R.id.txtIdentifyTV,
        R.id.txtDescribeTV,
        R.id.imgBackIV,
        R.id.txtSaveTV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.txtIdentifyTV -> bottomSheetIdentifyDialogue()
            R.id.txtInterestTV -> bottomSheetInterestDialogue()
            R.id.txtDescribeTV -> performDescribeClick()
            R.id.imgBackIV -> onBackPressed()
            R.id.txtSaveTV -> performSaveClick()
        }
    }

    private fun performDescribeClick() {
        val intent = Intent(mActivity, DescribeActivity::class.java)
        startActivity(intent)
    }

    private fun performSaveClick() {
        if (isNetworkAvailable(mActivity)) {
            executeUpdateProfileApi()
        } else
            showToast(mActivity, getString(R.string.internal_server_error))
    }

    // - - Identify Bottom Sheet Dialog
    private fun bottomSheetIdentifyDialogue() {
        val mBottomSheetDialog = BottomSheetDialog(mActivity)
        // on below line we are inflating a layout file which we have created.
        val view = layoutInflater.inflate(R.layout.dialog_bottom_sheet_identify, null)
        mBottomSheetDialog.setCancelable(true)
        mBottomSheetDialog.setContentView(view)
        mBottomSheetDialog.show()

        val txtHeTV: TextView = view.findViewById(R.id.txtHeTV)
        txtHeTV.setOnClickListener {
            mGender = "0"
            txtIdentifyTV.text = "He / Him"
            mBottomSheetDialog.dismiss()
        }
        val txtSheTV: TextView = view.findViewById(R.id.txtSheTV)
        txtSheTV.setOnClickListener {
            mGender = "1"
            txtIdentifyTV.text = "She / Her"
            mBottomSheetDialog.dismiss()
        }
        val txtTheyTV: TextView = view.findViewById(R.id.txtTheyTV)
        txtTheyTV.setOnClickListener {
            mGender = "2"
            txtIdentifyTV.text = "They / Them"
            mBottomSheetDialog.dismiss()
        }
    }

    // - - Interest Bottom Sheet Dialog
    private fun bottomSheetInterestDialogue() {
        val mBottomSheetDialog = BottomSheetDialog(mActivity)
        // on below line we are inflating a layout file which we have created.
        val view = layoutInflater.inflate(R.layout.dialog_bottom_sheet_interest, null)
        mBottomSheetDialog.setCancelable(true)
        mBottomSheetDialog.setContentView(view)
        mBottomSheetDialog.show()
        val txtForgiveMyselfTV: TextView = view.findViewById(R.id.txtForgiveMyselfTV)
        txtForgiveMyselfTV.setOnClickListener {
            mInterest = "0"
            txtInterestTV.text = "I want to forgive myself."
            mBottomSheetDialog.dismiss()
        }
        val txtForgiveOthersTV: TextView = view.findViewById(R.id.txtForgiveOthersTV)
        txtForgiveOthersTV.setOnClickListener {
            mInterest = "1"
            txtInterestTV.text = "I want to forgive others."
            mBottomSheetDialog.dismiss()
        }
        val txtForgiveBothTV: TextView = view.findViewById(R.id.txtForgiveBothTV)
        txtForgiveBothTV.setOnClickListener {
            mInterest = "2"
            txtInterestTV.text = "Both."
            mBottomSheetDialog.dismiss()
        }
    }


    // - - Execute Get Profile Api
    private fun executeGetProfileApi() {
        showProgressDialog(mActivity)
        getProfileViewModel?.getProfileData(mActivity, getAccessToken())?.observe(this,
            androidx.lifecycle.Observer<GetProfileModel?> { mModel ->
                if (mModel.status == 1) {
                    dismissProgressDialog()
                    AppPreference().writeString(this, NAME, mModel?.userDetail!!.userName)
                    AppPreference().writeString(this, PERSON_TYPE, mModel?.userDetail!!.personType)
                    setDataOnWidgets(mModel)
                } else {
                    showAlertDialog(mActivity, mModel.message)
                    dismissProgressDialog()
                }
            })
    }

    //      Set Data on Widgets
    private fun setDataOnWidgets(mModel: GetProfileModel) {
        editAgeEt.setText(mModel.userDetail?.userAge)
        txtDescribeTV.text = mModel.userDetail?.personType
        when (mModel.userDetail?.userGender) {
            "0" -> {
                txtIdentifyTV.text = "He / Him"
            }
            "1" -> {
                txtIdentifyTV.text = "She / Her"
            }
            "2" -> {
                txtIdentifyTV.text = "They / Them"
            }
        }
        when (mModel.userDetail?.userInterest) {
            "0" -> {
                txtInterestTV.text = "I want to forgive myself."
            }
            "1" -> {
                txtInterestTV.text = "I want to forgive others."
            }
            "2" -> {
                txtInterestTV.text = "Both."
            }
        }
    }

    // - - Execute Update Profile Request
    private fun executeUpdateProfileApi() {
        showProgressDialog(mActivity)
        updateProfileViewModel?.updateProfileData(
            mActivity,
            "1",
            getName(),
            editAgeEt.text.toString(),
            mInterest.toString(),
            mGender.toString(),
            getPersonType(),
            getAccessToken(),
            "Android",
            "Android"
        )?.observe(this,
            androidx.lifecycle.Observer<UpdateProfileModel?> { mModel ->
                if (mModel.status == 1) {
                    dismissProgressDialog()
                    executeGetProfileApi()
                    showToast(mActivity,"Updated Successfully")
                } else {
                    showAlertDialog(mActivity, mModel.message)
                    dismissProgressDialog()
                }
            })
    }

    override fun onRestart() {
        super.onRestart()
        txtDescribeTV.text = getPersonType()
    }
    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.right_to_left,R.anim.left_to_right)
    }
}