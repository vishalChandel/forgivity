package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.util.LINK_PP
import com.forgivity.app.util.LINK_TERMS
import com.forgivity.app.util.LINK_TYPE

class SubscriptionActivity2 : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription2)
        ButterKnife.bind(this)
    }

    @OnClick(
        R.id.txtFeesTV,
        R.id.txtTermsTV,
        R.id.txtPrivacyTV

        )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtFeesTV -> performClick()
            R.id.txtTermsTV -> performTermsConditionsClick()
            R.id.txtPrivacyTV -> performPrivacyPolicyClick()
        }
    }

    private fun performPrivacyPolicyClick() {
        val i = Intent(mActivity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_PP)
        startActivity(i)
    }

    private fun performTermsConditionsClick() {
        val i = Intent(mActivity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_TERMS)
        startActivity(i)
    }

    private fun performClick() {
        startActivity(Intent(mActivity, MyProgramActivity::class.java))
        finish()
    }
}