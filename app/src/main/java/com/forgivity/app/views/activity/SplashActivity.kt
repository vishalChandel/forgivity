package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import com.forgivity.app.R
import com.forgivity.app.util.SPLASH_TIME_OUT

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setUpSplash()
    }

    private fun setUpSplash() {
        val mThread = object : Thread() {
            override fun run() {
                sleep(SPLASH_TIME_OUT)
                if (isLogin()) {
                    val i = Intent(mActivity, MyProgramActivity::class.java)
                    startActivity(i)
                    finish()
                } else {
                    val i = Intent(mActivity, IntroActivity::class.java)
                    startActivity(i)
                    finish()
                }
            }
        }
        mThread.start()
    }

}

