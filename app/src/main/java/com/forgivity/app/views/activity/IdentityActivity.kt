package com.forgivity.app.views.activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.util.AppPreference
import com.forgivity.app.util.GENDER
import kotlinx.android.synthetic.main.activity_identity.*


class IdentityActivity : BaseActivity() {
// - - Initialize objects
    var identitySelection:Int?=4

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_identity)
        ButterKnife.bind(this)
        identitySelection()
    }

    private fun identitySelection() {
        txtHeTV.setOnClickListener {
            identitySelection=0
            txtSheTV.setBackgroundResource(R.drawable.edittxt_bg)
            txtSheTV.setTextColor(Color.parseColor("#6E39A8"))
            txtHeTV.setBackgroundResource(R.drawable.selected_bg_1)
            txtHeTV.setTextColor(Color.parseColor("#FFFFFF"))
            txtTheyTV.setBackgroundResource(R.drawable.edittxt_bg)
            txtTheyTV.setTextColor(Color.parseColor("#6E39A8"))
            AppPreference().writeString(this, GENDER, "0")
        }

        txtSheTV.setOnClickListener {
            identitySelection=1
            txtSheTV.setBackgroundResource(R.drawable.selected_bg_1)
            txtSheTV.setTextColor(Color.parseColor("#FFFFFF"))
            txtHeTV.setBackgroundResource(R.drawable.edittxt_bg)
            txtHeTV.setTextColor(Color.parseColor("#6E39A8"))
            txtTheyTV.setBackgroundResource(R.drawable.edittxt_bg)
            txtTheyTV.setTextColor(Color.parseColor("#6E39A8"))
            AppPreference().writeString(this, GENDER, "1")
        }

        txtTheyTV.setOnClickListener {
            identitySelection=2
            txtSheTV.setBackgroundResource(R.drawable.edittxt_bg)
            txtSheTV.setTextColor(Color.parseColor("#6E39A8"))
            txtHeTV.setBackgroundResource(R.drawable.edittxt_bg)
            txtHeTV.setTextColor(Color.parseColor("#6E39A8"))
            txtTheyTV.setBackgroundResource(R.drawable.selected_bg_1)
            txtTheyTV.setTextColor(Color.parseColor("#FFFFFF"))
            AppPreference().writeString(this, GENDER, "2")
        }
    }

    @OnClick(
        R.id.btnNextIdentity,
        R.id.btnBackIdentity
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.btnNextIdentity -> performNextClick()
            R.id.btnBackIdentity -> onBackPressed()
        }
    }

    private fun performNextClick() {
        if (identitySelection==4) {
            showAlertDialog(mActivity, "Please select one of these options.")
        }
        else {
            startActivity(Intent(mActivity, InterestActivity::class.java))
        }
    }
}