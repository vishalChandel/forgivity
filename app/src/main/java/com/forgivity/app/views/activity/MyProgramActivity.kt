package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.interfaces.LoadMoreSavedData
import com.forgivity.app.model.ListItem
import com.forgivity.app.model.SaveModel
import com.forgivity.app.model.SavedModel
import com.forgivity.app.model.UnSaveModel
import com.forgivity.app.viewModel.SaveViewModel
import com.forgivity.app.viewModel.SavedViewModel
import com.forgivity.app.viewModel.UnSaveViewModel
import kotlinx.android.synthetic.main.activity_my_program.*
import kotlinx.android.synthetic.main.activity_saved.*

class MyProgramActivity : BaseActivity() {
    // - - Initialize Objects
    private var saveViewModel: SaveViewModel? = null
    private var unSaveViewModel: UnSaveViewModel? = null
    private var isSaved:Boolean=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_program)
        ButterKnife.bind(this)
        saveViewModel = ViewModelProviders.of(this).get(SaveViewModel::class.java)
        unSaveViewModel = ViewModelProviders.of(this).get(UnSaveViewModel::class.java)
    }

    @OnClick(
        R.id.btnGotItHome1,
        R.id.btnBackHome,
        R.id.drawerRL,
        R.id.imgSaveIV,
        R.id.txtRelatedListeningTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.btnGotItHome1 -> performGotItClick()
            R.id.drawerRL -> performDrawerClick()
            R.id.imgSaveIV -> performSaveClick()
            R.id.txtRelatedListeningTV -> performRelatedListeningClick()
            R.id.btnBackHome -> performBackClick()
        }
    }

    private fun performSaveClick() {
        when {
            !isSaved -> {
                if (isNetworkAvailable(mActivity)) {
                    executeSaveDataRequest()
                } else {
                    showAlertDialog(mActivity, getString(R.string.internet_connection_error))
                }}
            isSaved -> {
                if (isNetworkAvailable(mActivity)) {
                    executeUnSaveDataRequest()
                } else {
                    showAlertDialog(mActivity, getString(R.string.internet_connection_error))
                }
            }
        }
    }

    private fun performBackClick() {
//        showSurveyAlertDialog(mActivity)
    }

    private fun performRelatedListeningClick() {
    }

    private fun performGotItClick() {

    }

    private fun performDrawerClick() {
        startActivity(Intent(mActivity, HomeActivity::class.java))
        overridePendingTransition(
            R.anim.right_to_left,
            R.anim.left_to_right
        )
    }

    // - - Execute Save Data Api
    private fun executeSaveDataRequest() {
        showProgressDialog(mActivity)
        saveViewModel?.saveData(mActivity, "user_908239719", "7","day")
            ?.observe(this,
                androidx.lifecycle.Observer<SaveModel?> { mModel ->
                    when (mModel.status) {
                        1 -> {
                            dismissProgressDialog()
                            imgSaveIV.setImageResource(R.drawable.ic_bookmark)
                            isSaved=true
                            showToast(mActivity,mModel.message)
                        }
                        else -> {
                            showAlertDialog(mActivity, mModel.message)
                            dismissProgressDialog()
                        }
                    }
                })
    }

    // - - Execute UnSave Data Api
    private fun executeUnSaveDataRequest() {
        showProgressDialog(mActivity)
        unSaveViewModel?.unSaveData(mActivity, "user_908239719", "7","day")
            ?.observe(this,
                androidx.lifecycle.Observer<UnSaveModel?> { mModel ->
                    when (mModel.status) {
                        1 -> {
                            dismissProgressDialog()
                            imgSaveIV.setImageResource(R.drawable.ic_save)
                            isSaved=false
                            showToast(mActivity,mModel.message)
                        }
                        else -> {
                            showAlertDialog(mActivity, mModel.message)
                            dismissProgressDialog()
                        }
                    }
                })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}