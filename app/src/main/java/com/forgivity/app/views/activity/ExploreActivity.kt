package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.adapter.DayExploreAdapter
import com.forgivity.app.adapter.GlossaryExploreAdapter
import com.forgivity.app.adapter.PodcastExploreAdapter
import com.forgivity.app.interfaces.LoadMoreDaysData
import com.forgivity.app.interfaces.LoadMoreGlossaryData
import com.forgivity.app.interfaces.LoadMorePodcastData
import com.forgivity.app.model.*
import com.forgivity.app.viewModel.DayViewModel
import com.forgivity.app.viewModel.GlossaryViewModel
import com.forgivity.app.viewModel.PodcastViewModel
import kotlinx.android.synthetic.main.activity_explore.*


class ExploreActivity : BaseActivity(), LoadMorePodcastData, LoadMoreGlossaryData,
    LoadMoreDaysData {

    // - - Initialize Objects
    private var mPageNoGlossary: Int = 1
    private var mPageNoDay: Int = 1
    private var mPageNoPodcast: Int = 1
    private var isLoading: Boolean = true
    private var mLoadMoreDaysData: LoadMoreDaysData? = null
    private var mLoadMoreGlossaryData: LoadMoreGlossaryData? = null
    private var mLoadMorePodcastData: LoadMorePodcastData? = null
    private var mDaysList: ArrayList<ListItemDay?>? = ArrayList()
    private var mGlossaryList: ArrayList<ListItemGlossary?>? = ArrayList()
    private var mPodcastList: ArrayList<ListItemPodcast?>? = ArrayList()
    private var glossaryExploreAdapter: GlossaryExploreAdapter? = null
    private var podcastExploreAdapter: PodcastExploreAdapter? = null
    private var dayExploreAdapter: DayExploreAdapter? = null
    private var dayViewModel: DayViewModel? = null
    private var podcastViewModel: PodcastViewModel? = null
    private var glossaryViewModel: GlossaryViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_explore)
        ButterKnife.bind(this)
        dayViewModel = ViewModelProviders.of(this).get(DayViewModel::class.java)
        podcastViewModel = ViewModelProviders.of(this).get(PodcastViewModel::class.java)
        glossaryViewModel = ViewModelProviders.of(this).get(GlossaryViewModel::class.java)
        performPodDropsClick()
    }

    @OnClick(
        R.id.imgDrawer,
        R.id.podDropsRL,
        R.id.glossaryRL,
        R.id.daysRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgDrawer -> performDrawerClick()
            R.id.podDropsRL -> performPodDropsClick()
            R.id.glossaryRL -> performGlossaryClick()
            R.id.daysRL -> performDaysClick()
        }
    }

    private fun performDrawerClick() {
        startActivity(Intent(mActivity, HomeActivity::class.java))
        overridePendingTransition(
            R.anim.right_to_left,
            R.anim.left_to_right
        )
    }

    private fun performDaysClick() {
        mPageNoGlossary = 1
        mPageNoDay = 1
        mPageNoPodcast = 1
        daysRL.setBackgroundResource(R.drawable.poddrops_rl_bg)
        glossaryRL.setBackgroundResource(R.drawable.poddrops_rl_light_bg)
        podDropsRL.setBackgroundResource(R.drawable.poddrops_rl_light_bg)
        explorePodcastRV.visibility = View.GONE
        exploreGlossaryRV.visibility = View.GONE
        exploreDayRV.visibility = View.VISIBLE
        noMoreDataTV.visibility = View.GONE
        mLoadMoreDaysData = this
        if (mDaysList != null) {
            mDaysList!!.clear()
        }
        getDaysData()
        editSearchET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                editSearchET.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        mDaysList?.clear()
                        dayExploreAdapter?.notifyDataSetChanged()
                        executeDayApi()
                        return@OnEditorActionListener true
                    } else
                        false
                })
            }
        })
    }


    private fun performGlossaryClick() {
        mPageNoGlossary = 1
        mPageNoDay = 1
        mPageNoPodcast = 1
        daysRL.setBackgroundResource(R.drawable.poddrops_rl_light_bg)
        glossaryRL.setBackgroundResource(R.drawable.poddrops_rl_bg)
        podDropsRL.setBackgroundResource(R.drawable.poddrops_rl_light_bg)
        exploreDayRV.visibility = View.GONE
        explorePodcastRV.visibility = View.GONE
        exploreGlossaryRV.visibility = View.VISIBLE
        noMoreDataTV.visibility = View.GONE
        mLoadMoreGlossaryData = this
        if (mGlossaryList != null) {
            mGlossaryList!!.clear()
        }
        getGlossaryData()
        editSearchET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                editSearchET.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        mGlossaryList?.clear()
                        glossaryExploreAdapter?.notifyDataSetChanged()
                        executeGlossaryApi()
                        return@OnEditorActionListener true
                    } else
                        false
                })
            }
        })
    }

    private fun performPodDropsClick() {
        mPageNoGlossary = 1
        mPageNoDay = 1
        mPageNoPodcast = 1
        daysRL.setBackgroundResource(R.drawable.poddrops_rl_light_bg)
        glossaryRL.setBackgroundResource(R.drawable.poddrops_rl_light_bg)
        podDropsRL.setBackgroundResource(R.drawable.poddrops_rl_bg)
        exploreDayRV.visibility = View.GONE
        exploreGlossaryRV.visibility = View.GONE
        noMoreDataTV.visibility = View.GONE
        explorePodcastRV.visibility = View.VISIBLE
        mLoadMorePodcastData = this
        if (mPodcastList != null) {
            mPodcastList!!.clear()
        }
        getPodcastData()
        editSearchET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                Log.e("search text before", s.toString())
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
            override fun afterTextChanged(s: Editable?) {
                editSearchET.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        mPodcastList?.clear()
                        podcastExploreAdapter?.notifyDataSetChanged()
                        executePodcastApi()
                        return@OnEditorActionListener true
                    } else
                        false
                })
            }
        })
    }


    private fun getPodcastData() {
        if (isNetworkAvailable(mActivity)) {
            executePodcastApi()
        } else {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun getGlossaryData() {
        if (isNetworkAvailable(mActivity)) {
            executeGlossaryApi()
        } else {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun getDaysData() {
        if (isNetworkAvailable(mActivity)) {
            executeDayApi()
        } else {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun setGlossaryExploreAdapter(mModel: GlossaryModel) {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        exploreGlossaryRV.layoutManager = layoutManager
        glossaryExploreAdapter =
            GlossaryExploreAdapter(mActivity, mGlossaryList, mLoadMoreGlossaryData, mModel)
        exploreGlossaryRV.adapter = glossaryExploreAdapter
    }

    private fun setDayExploreAdapter(mModel: DayModel) {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        exploreDayRV.layoutManager = layoutManager
        dayExploreAdapter = DayExploreAdapter(mActivity, mDaysList, mLoadMoreDaysData, mModel)
        exploreDayRV.adapter = dayExploreAdapter
    }

    private fun setPodcastExploreAdapter(mModel: PodcastModel) {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        explorePodcastRV.layoutManager = layoutManager
        podcastExploreAdapter =
            PodcastExploreAdapter(mActivity, mPodcastList, mLoadMorePodcastData, mModel)
        explorePodcastRV.adapter = podcastExploreAdapter
    }

    // - - Execute Glossary Request
    private fun executeGlossaryApi() {
        showProgressDialog(mActivity)
        glossaryViewModel?.glossaryData(
            mActivity,
            getAccessToken(),
            "glossary",
            mPageNoGlossary.toString(),
            editSearchET.text.toString()
        )
            ?.observe(this,
                androidx.lifecycle.Observer<GlossaryModel?> { mModel ->
                    if (mModel.status == 1) {
                        dismissProgressDialog()
                        isLoading = !mModel.allData?.lastPage?.equals(true)!!
                        mGlossaryList = mModel!!.allData?.list
                        setGlossaryExploreAdapter(mModel)
                        noMoreDataTV.visibility = View.GONE
                    } else {
                        dismissProgressDialog()
                        noMoreDataTV.visibility = View.VISIBLE
                    }
                })
    }

    // - - Execute Day Request
    private fun executeDayApi() {
        showProgressDialog(mActivity)
        dayViewModel?.dayData(
            mActivity,
            getAccessToken(),
            "day",
            mPageNoDay.toString(),
            editSearchET.text.toString()
        )
            ?.observe(this,
                androidx.lifecycle.Observer<DayModel?> { mModel ->
                    if (mModel.status == 1) {
                        dismissProgressDialog()
                        isLoading = !mModel.allData?.lastPage?.equals(true)!!
                        mDaysList = mModel!!.allData?.list
                        setDayExploreAdapter(mModel)
                        noMoreDataTV.visibility = View.GONE

                    } else {
                        dismissProgressDialog()
                        noMoreDataTV.visibility = View.VISIBLE
                    }
                })
    }

    // - - Execute Podcast Request
    private fun executePodcastApi() {
        showProgressDialog(mActivity)
        podcastViewModel?.podcastData(
            mActivity,
            getAccessToken(),
            "podcast",
            mPageNoPodcast.toString(),
            editSearchET.text.toString()
        )
            ?.observe(this,
                androidx.lifecycle.Observer<PodcastModel?> { mModel ->
                    if (mModel.status == 1) {
                        dismissProgressDialog()
                        isLoading = !mModel.allData?.lastPage?.equals(true)!!
                        mPodcastList = mModel!!.allData?.list
                        setPodcastExploreAdapter(mModel)
                        noMoreDataTV.visibility = View.GONE
                    } else {
                        dismissProgressDialog()
                        noMoreDataTV.visibility = View.VISIBLE
                    }
                })
    }

    private fun loadMorePodcastData() {
        mProgressRL.visibility = View.VISIBLE
        podcastViewModel?.podcastData(
            mActivity,
            getAccessToken(),
            "podcast",
            mPageNoPodcast.toString(),
            editSearchET.text.toString()
        )
            ?.observe(this, { mModel ->

                if (mModel.status == 1) {
                    mProgressRL.visibility = View.GONE
                    mModel.allData?.list?.let { mPodcastList?.addAll(it) }
                    podcastExploreAdapter?.notifyDataSetChanged()
                    isLoading = !mModel.allData?.lastPage?.equals(true)!!
                } else if (mModel.status == 0) {
                    mProgressRL.visibility = View.GONE
                }
            })

    }

    private fun loadMoreDayData() {
        mProgressRL.visibility = View.VISIBLE
        dayViewModel?.dayData(
            mActivity,
            getAccessToken(),
            "day",
            mPageNoDay.toString(),
            editSearchET.text.toString()
        )
            ?.observe(this, androidx.lifecycle.Observer<DayModel?> { mModel ->

                if (mModel.status == 1) {
                    mProgressRL.visibility = View.GONE
                    mModel.allData?.list?.let { mDaysList?.addAll(it) }
                    dayExploreAdapter?.notifyDataSetChanged()
                    isLoading = !mModel.allData?.lastPage?.equals(true)!!
                } else if (mModel.status == 0) {
                    mProgressRL.visibility = View.GONE
                }
            })

    }

    private fun loadMoreGlossaryData() {
        mProgressRL.visibility = View.VISIBLE
        glossaryViewModel?.glossaryData(
            mActivity,
            getAccessToken(),
            "glossary",
            mPageNoGlossary.toString(),
            editSearchET.text.toString()
        )
            ?.observe(this, { mModel ->

                if (mModel.status == 1) {
                    mProgressRL.visibility = View.GONE
                    mModel.allData?.list?.let { mGlossaryList?.addAll(it) }
                    glossaryExploreAdapter?.notifyDataSetChanged()
                    isLoading = !mModel.allData?.lastPage?.equals(true)!!
                } else if (mModel.status == 0) {
                    mProgressRL.visibility = View.GONE
                }
            })

    }

    override fun onLoadMorePodcastData(mModel: ListItemPodcast) {
        if (isLoading) {
            ++mPageNoPodcast
            loadMorePodcastData()
        }
    }

    override fun onLoadMoreGlossaryData(mModel: ListItemGlossary) {
        if (isLoading) {
            ++mPageNoGlossary
            loadMoreGlossaryData()
        }
    }

    override fun onLoadMoreDaysData(mModel: ListItemDay) {
        if (isLoading) {
            ++mPageNoDay
            loadMoreDayData()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right)
    }
}