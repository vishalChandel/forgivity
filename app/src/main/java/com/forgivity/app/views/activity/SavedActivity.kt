package com.forgivity.app.views.activity

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.adapter.SavedAdapter
import com.forgivity.app.interfaces.LoadMoreSavedData
import com.forgivity.app.model.ListItem
import com.forgivity.app.model.SavedModel
import com.forgivity.app.viewModel.SavedViewModel
import kotlinx.android.synthetic.main.activity_saved.*


class SavedActivity : BaseActivity(), LoadMoreSavedData {
    // - - Initialize Objects
    var mLoadMore: LoadMoreSavedData? = null
    var mPageNo: Int = 1
    var isLoading: Boolean = true
    var mSavedList: ArrayList<ListItem?>? = ArrayList()
    private var savedViewModel: SavedViewModel? = null
    private var savedAdapter: SavedAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_saved)
        ButterKnife.bind(this)
        setEditTextFocused(editSearchET)
        savedViewModel = ViewModelProviders.of(this).get(SavedViewModel::class.java)
        mLoadMore = this
        if (mSavedList != null) {
            mSavedList!!.clear()
        }
        getSavedData()
        SearchItem()
    }

    private fun getSavedData() {
        if (isNetworkAvailable(mActivity)) {
            executeSavedRequest()
        } else {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        }
    }

    @OnClick(
        R.id.imgBackIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
        }
    }

    // - - Execute Activity List Api
    private fun executeSavedRequest() {
        showProgressDialog(mActivity)
        savedViewModel?.savedListData(mActivity, "user_908239719", mPageNo.toString())
            ?.observe(this,
                androidx.lifecycle.Observer<SavedModel?> { mModel ->
                    when (mModel.status) {
                        1 -> {
                            dismissProgressDialog()
                            isLoading = !mModel.allData?.lastPage?.equals(true)!!
                            mSavedList = mModel!!.allData?.list
                            setAdapter()
                            noMoreDataTV.visibility = View.GONE
                        }
                        else -> {
                            showAlertDialog(mActivity, mModel.message)
                            dismissProgressDialog()
                            noMoreDataTV.visibility = View.VISIBLE
                        }
                    }
                })
    }

    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        savedRV.layoutManager = layoutManager
        savedAdapter = SavedAdapter(mActivity, mSavedList, mLoadMore)
        savedRV.adapter = savedAdapter
    }

    private fun loadMoreSavedData() {
        mProgressRL.visibility = View.VISIBLE
        savedViewModel?.savedListData(mActivity, "user_908239719", mPageNo.toString())
            ?.observe(this, androidx.lifecycle.Observer<SavedModel?> { mModel ->

                if (mModel.status == 1) {
                    mProgressRL.visibility = View.GONE
                    mModel.allData?.list?.let { mSavedList?.addAll(it) }
                    savedAdapter?.notifyDataSetChanged()
                    isLoading = !mModel.allData?.lastPage?.equals(true)!!
                } else if (mModel.status == 0) {
                    mProgressRL.visibility = View.GONE
                }
            })
    }


    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right)
    }

    override fun onLoadMoreSavedData(mModel: ListItem) {
        if (isLoading) {
            ++mPageNo
            loadMoreSavedData()
        }
    }

    private fun SearchItem() {
        editSearchET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                //after the change calling the method and passing the search input
                if (editable.toString().isEmpty()) {
                    mSavedList?.let { savedAdapter?.filterList(it) }
                } else {
                    filter(editable.toString())
                }
            }
        })
    }

    fun filter(text: String?) {
        val temp: ArrayList<ListItem?> = ArrayList()
        for (d in mSavedList!!) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            when {
                d?.title?.contains(text.toString(),true)!! -> {
                    temp.add(d)
                }
                d?.desc?.contains(text.toString(),true)!! -> {
                    temp.add(d)
                }
            }
        }
        //update recyclerview
        savedAdapter?.filterList(temp)
    }
}


