package com.forgivity.app.views.activity

import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.StrictMode
import android.view.View
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import kotlinx.android.synthetic.main.activity_audio.*
import android.view.View.OnTouchListener

class AudioActivity : BaseActivity(), MediaPlayer.OnCompletionListener {

    // - - Initialize Objects
    var mMediaPlayer: MediaPlayer? = null
    private var audioManager: AudioManager? = null
    private var isPlaying = false
    private var audioFile: String = ""
    private var audioFileName: String = ""
    private val myHandler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio)
        ButterKnife.bind(this)
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m =
                    StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
        showProgressDialog(mActivity)
        volumeControlStream = AudioManager.STREAM_MUSIC
        getIntentData()
        initialiseMediaPlayer()
    }

    private fun getIntentData() {
        if (intent != null) {
            audioFile = audioFile.let { intent.getStringExtra("audioFile").toString() }
            audioFileName = audioFileName.let { intent.getStringExtra("audioFileName").toString() }
            txtAudioTitleTV.text = audioFileName
        }

    }

    @OnClick(
        R.id.imgCrossIV,
        R.id.imgPlayPauseAudioIV,
        R.id.imgBackwardIV,
        R.id.imgForwardIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgCrossIV -> onBackPressed()
            R.id.imgPlayPauseAudioIV -> performPlayPauseAudioClick()
            R.id.imgBackwardIV -> performBackwardAudioClick()
            R.id.imgForwardIV -> performForwardAudioClick()
        }
    }

    private fun performForwardAudioClick() {
        val currentAudioDuration = mMediaPlayer!!.currentPosition.toLong() + 5000
        mMediaPlayer!!.seekTo(currentAudioDuration.toInt())
    }

    private fun performBackwardAudioClick() {
        val currentAudioDuration = mMediaPlayer!!.currentPosition.toLong() - 5000
        mMediaPlayer!!.seekTo(currentAudioDuration.toInt())
    }

    private fun startAudio() {
        val myUri: Uri? = Uri.parse(audioFile)
        if (myUri != null) {
            mMediaPlayer!!.setDataSource(audioFile)
            mMediaPlayer!!.prepare()
            mMediaPlayer!!.setOnPreparedListener {
                it.start()
                isPlaying = true
                if(mMediaPlayer!!.isPlaying){
                    dismissProgressDialog()
                    imgPlayPauseAudioIV.setImageResource(R.drawable.ic_pause_audio)
                }
                audioTimerSB.progress = 0
                audioTimerSB.max = 100
                audioTimerSB.setOnTouchListener(OnTouchListener { v, event -> true })
                updateProgressBar()
            }
        }
    }

    //Audio player functions
    private fun updateProgressBar() {
        myHandler.postDelayed(UpdateTimeTask, 100)
    }

    private val UpdateTimeTask: Runnable = object : Runnable {
        override fun run() {
            val totalduration = mMediaPlayer!!.duration.toLong()
            val currentduration = mMediaPlayer!!.currentPosition.toLong()
            startTimerTV!!.text = "" + milliSecondstoTimer(currentduration)
            endTimerTV.text = "" + milliSecondstoTimer(totalduration)
            audioTimerSB!!.progress = getProgressPercentage(
                currentduration,
                totalduration
            )
            myHandler.postDelayed(this, 100)
        }
    }

    fun milliSecondstoTimer(milliseconds: Long): String? {
        var finalTimerString = ""
        var secondsString = ""
        var minuteString = ""
        val hours = (milliseconds / (1000 * 60 * 60)).toInt()
        val minutes = (milliseconds % (1000 * 60 * 60)).toInt() / (1000 * 60)
        val seconds = (milliseconds % (1000 * 60 * 60) % (1000 * 60) / 1000).toInt()
        if (hours > 0) {
            finalTimerString = if (hours < 10) {
                "0$hours:"
            } else "" + hours
        }
        secondsString = if (seconds < 10) {
            "0$seconds"
        } else "" + seconds

        minuteString = if (minutes < 10) {
            "0$minutes"
        } else "" + minutes
        finalTimerString = "$finalTimerString$minuteString:$secondsString"
        return finalTimerString
    }

    fun getProgressPercentage(currentduration: Long, totalduration: Long): Int {
        var percentage = 0.toDouble()
        val currentseconds = currentduration.toInt() / 1000.toLong()
        val totalseconds = totalduration.toInt() / 1000.toLong()
        percentage = currentseconds.toDouble() / totalseconds * 100
        return percentage.toInt()
    }

    private fun initialiseMediaPlayer() {
        val thread = Thread {
            try {
                mMediaPlayer = MediaPlayer()
                mMediaPlayer!!.setOnCompletionListener(this)
                val ama = getSystemService(AUDIO_SERVICE) as AudioManager
                // Request audio focus for playback
                val result = ama.requestAudioFocus(
                    focusChangeListener,  // Use the music stream.
                    AudioManager.STREAM_MUSIC,  // Request permanent focus.
                    AudioManager.AUDIOFOCUS_GAIN
                )
                initAudioControls()
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    // other app had stopped playing song now , so u can do your stuff now .
                }
                startAudio()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        thread.start()



    }

    private fun initAudioControls() {
        try {
            audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
            audioSB!!.max = audioManager!!
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC)
            audioSB!!.progress = audioManager!!
                .getStreamVolume(AudioManager.STREAM_MUSIC)
            audioSB!!.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
                override fun onStopTrackingTouch(arg0: SeekBar) {}
                override fun onStartTrackingTouch(arg0: SeekBar) {}
                override fun onProgressChanged(arg0: SeekBar, progress: Int, arg2: Boolean) {
                    audioManager!!.setStreamVolume(
                        AudioManager.STREAM_MUSIC,
                        progress, 0
                    )
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun performPlayPauseAudioClick() {
        if (isPlaying) {
            pauseAudio()
        } else if (!isPlaying) {
            playAudio()
        }
    }

    override fun onCompletion(mMediaPlayer: MediaPlayer?) {
        try {
            mMediaPlayer!!.pause()
            imgPlayPauseAudioIV.setImageResource(R.drawable.ic_play)
            isPlaying=false
            mMediaPlayer!!.reset()
        } catch (e: Exception) {

        }
    }

    private val focusChangeListener = AudioManager.OnAudioFocusChangeListener { focusChange ->
        try {
            val mediaPlayerBackground = MediaPlayer()

            when (focusChange) {
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK ->                                 // Lower the volume while ducking.
                {
                    mediaPlayerBackground.setVolume(0.2f, 0.2f)
                    pauseAudio()
                }
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> {
                    mediaPlayerBackground.stop()
                    pauseAudio()
                }
                AudioManager.AUDIOFOCUS_LOSS -> {
                    mediaPlayerBackground.stop()
                    pauseAudio()
                }
                AudioManager.AUDIOFOCUS_GAIN -> {
                    // Return the volume to normal and resume if paused.
                    mediaPlayerBackground.setVolume(1f, 1f)
                    mediaPlayerBackground.start()
                }
                else -> {
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun pauseAudio() {
        if (mMediaPlayer != null) {
            mMediaPlayer!!.pause()
            isPlaying = false
            imgPlayPauseAudioIV.setImageResource(R.drawable.ic_play_rl)
        }
    }

    private fun playAudio() {
        if (mMediaPlayer != null) {
            mMediaPlayer!!.start()
            isPlaying = true
            imgPlayPauseAudioIV.setImageResource(R.drawable.ic_pause_audio)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        pauseAudio()
        mMediaPlayer!!.reset()
    }
}