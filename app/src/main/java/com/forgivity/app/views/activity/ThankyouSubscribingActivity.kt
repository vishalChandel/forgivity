package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.util.AGE
import com.forgivity.app.util.AppPreference
import kotlinx.android.synthetic.main.activity_birthday.*
import kotlinx.android.synthetic.main.activity_name.*


class ThankyouSubscribingActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_thankyou_subscribing)
        ButterKnife.bind(this)
    }

    @OnClick(
        R.id.txtNextTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtNextTV -> performNextClick()
        }
    }

    private fun performNextClick() {
            startActivity(Intent(mActivity, MyProgramActivity::class.java))
        finish()

        }

    override fun onBackPressed() {
        super.onBackPressed()
    }

}
