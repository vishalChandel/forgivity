package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R

class WelcomeActivity2 : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome2)
        ButterKnife.bind(this)
    }

    @OnClick(
        R.id.btnGotItWelcome2,
        R.id.btnBackWelcome2
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.btnGotItWelcome2 -> performGotItClick()
            R.id.btnBackWelcome2 -> onBackPressed()
        }
    }
    private fun performGotItClick() {
        startActivity(Intent(mActivity, SubscriptionActivity1::class.java))
        finish()
    }
}