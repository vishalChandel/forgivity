package com.forgivity.app.views.activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.util.AppPreference
import com.forgivity.app.util.INTEREST
import kotlinx.android.synthetic.main.activity_interest.*

class InterestActivity : BaseActivity() {
    // - - Initialize Objects
    var interestSelection:Int?=4

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_interest)
        ButterKnife.bind(this)
        interestSelection()
    }

    private fun interestSelection() {
        txtMyselfTV.setOnClickListener {
            interestSelection=0
            txtMyselfTV.setBackgroundResource(R.drawable.selected_bg_1)
            txtMyselfTV.setTextColor(Color.parseColor("#FFFFFF"))
            txtOtherTV.setBackgroundResource(R.drawable.edittxt_bg)
            txtOtherTV.setTextColor(Color.parseColor("#6E39A8"))
            txtBothTV.setBackgroundResource(R.drawable.edittxt_bg)
            txtBothTV.setTextColor(Color.parseColor("#6E39A8"))
            AppPreference().writeString(this, INTEREST,"0")
        }
        txtOtherTV.setOnClickListener {
            interestSelection=1
            txtMyselfTV.setBackgroundResource(R.drawable.edittxt_bg)
            txtMyselfTV.setTextColor(Color.parseColor("#6E39A8"))
            txtOtherTV.setBackgroundResource(R.drawable.selected_bg_1)
            txtOtherTV.setTextColor(Color.parseColor("#FFFFFF"))
            txtBothTV.setBackgroundResource(R.drawable.edittxt_bg)
            txtBothTV.setTextColor(Color.parseColor("#6E39A8") )
            AppPreference().writeString(this, INTEREST,"1")

        }
        txtBothTV.setOnClickListener {
            interestSelection=2
            txtMyselfTV.setBackgroundResource(R.drawable.edittxt_bg)
            txtMyselfTV.setTextColor(Color.parseColor("#6E39A8"))
            txtOtherTV.setBackgroundResource(R.drawable.edittxt_bg)
            txtOtherTV.setTextColor(Color.parseColor("#6E39A8"))
            txtBothTV.setBackgroundResource(R.drawable.selected_bg_1)
            txtBothTV.setTextColor(Color.parseColor("#FFFFFF"))
            AppPreference().writeString(this, INTEREST,"2")
        }
    }

    @OnClick(
        R.id.btnNextInterest,
        R.id.btnBackInterest
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.btnNextInterest -> performNextClick()
            R.id.btnBackInterest -> onBackPressed()
        }
    }

    private fun performNextClick() {
        if (interestSelection==4) {
            showAlertDialog(mActivity, "Please select one of these options.")
        } else {
            startActivity(Intent(mActivity, DescribeActivity::class.java))
        }
    }
}