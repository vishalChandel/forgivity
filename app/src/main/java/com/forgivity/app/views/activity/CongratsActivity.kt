package com.forgivity.app.views.activity

import android.os.Bundle
import android.view.animation.AnimationUtils
import com.bumptech.glide.Glide
import com.forgivity.app.R
import kotlinx.android.synthetic.main.activity_congrats.*
class CongratsActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_congrats)
        Glide.with(this).load(R.drawable.square_cuts).into(gifIV)
        congratsIV.startAnimation(AnimationUtils.loadAnimation(this, R.anim.continuous_zoom_in_zoom_out))
    }
}