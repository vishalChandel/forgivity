package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.util.AGE
import com.forgivity.app.util.AppPreference
import kotlinx.android.synthetic.main.activity_birthday.*
import kotlinx.android.synthetic.main.activity_name.*


class BirthdayActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_birthday)
        ButterKnife.bind(this)
        ageSeekBar()
    }

    @OnClick(
        R.id.btnNextBirthday,
        R.id.btnBackBirthday
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.btnNextBirthday -> performNextClick()
            R.id.btnBackBirthday -> onBackPressed()
        }
    }

    private fun performNextClick() {
        if (isValidate()) {
        startActivity(Intent(mActivity, IdentityActivity::class.java))
        AppPreference().writeString(this, AGE, txtAgeTV.text.toString())
    }}

    // - - Validations
    private fun isValidate(): Boolean {
        var flag = true
        if (Integer.parseInt(txtAgeTV!!.text.toString())<18) {
            showAlertDialog(mActivity, getString(R.string.please_enter_age))
            flag = false
        }
        return flag
    }

    private fun ageSeekBar() {
        birthdaySeekBar.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(
                seekBar: SeekBar, progress: Int,
                fromUser: Boolean
            ) {
                txtAgeTV.text = "" + progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

            }
        })
    }
}
