package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R

class StartActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        ButterKnife.bind(this)
    }
    @OnClick(
        R.id.txtLetsStartTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtLetsStartTV -> performLetsStartClick()
        }
    }

    private fun performLetsStartClick() {
        startActivity(Intent(mActivity, NameActivity::class.java))
    }
}