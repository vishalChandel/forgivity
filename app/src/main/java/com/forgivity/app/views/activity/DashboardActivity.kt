package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.model.DashboardModel
import com.forgivity.app.viewModel.DashboardViewModel
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : BaseActivity() {

    // - - Initialize Objects
    private var dashboardViewModel: DashboardViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        ButterKnife.bind(this)
        dashboardViewModel = ViewModelProviders.of(this).get(DashboardViewModel::class.java)
        executeDashboardApi()
    }

    @OnClick(
        R.id.imgDrawer

    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgDrawer -> performDrawerClick()
        }
    }

    private fun performDrawerClick() {
        startActivity(Intent(mActivity, HomeActivity::class.java))
        overridePendingTransition(
            R.anim.right_to_left,
            R.anim.left_to_right
        )
    }

    // - - Execute DashBoard Request
    private fun executeDashboardApi() {
        showProgressDialog(mActivity)
        dashboardViewModel?.dashboardData(mActivity, getAccessToken())?.observe(this,
            androidx.lifecycle.Observer<DashboardModel?> { mModel ->
                if (mModel.status == 1) {
                    dismissProgressDialog()
                    setDataOnWidgets(mModel)
                } else {
                    showAlertDialog(mActivity, mModel.message)
                    dismissProgressDialog()
                }
            })
    }

    //      Set Data on Widgets
    private fun setDataOnWidgets(mModel: DashboardModel) {
        txtCheckInTV.text = mModel.userDetail!!.checkIn.toString()
        txtTimeSpentTV.text = mModel.userDetail!!.timeSpent.toString()
        txtSavedToolsTV.text = mModel.userDetail!!.savedTools.toString()
        txtSavedDaysTV.text = mModel.userDetail!!.savedDays.toString()
        txtDaysCompletedTV.text = mModel.userDetail!!.dayCompleted.toString()
        txtLevelCompletedTV.text = mModel.userDetail!!.levelCompleted.toString()

    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.right_to_left,R.anim.left_to_right)
    }

}