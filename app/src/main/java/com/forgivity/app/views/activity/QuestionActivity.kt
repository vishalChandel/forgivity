package com.forgivity.app.views.activity

import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R

class QuestionActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_question)
        ButterKnife.bind(this)
    }
    @OnClick(
        R.id.btnYes,
        R.id.btnNo,
        R.id.imgDropDown
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.btnYes -> performYesClick()
            R.id.btnNo -> performNoClick()
            R.id.imgDropDown -> performDropDownClick()
        }
    }

    private fun performYesClick() {
        showCorrectAlertDialog(mActivity, getString(R.string.dummy_text))
    }
    private fun performNoClick() {
        showIncorrectAlertDialog(mActivity, getString(R.string.dummy_text))
    }

    private fun performDropDownClick() {
    }
}