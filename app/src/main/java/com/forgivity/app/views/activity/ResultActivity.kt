package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R

class ResultActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        ButterKnife.bind(this)
    }

    @OnClick(
        R.id.btnGoBack,
        R.id.imgBackIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.btnGoBack -> performGoBackClick()
            R.id.imgBackIV -> performBackClick()
        }
    }

    private fun performGoBackClick() {

        startActivity(Intent(mActivity, MyProgramActivity::class.java))
        finish()

    }

    private fun performBackClick() {

        startActivity(Intent(mActivity, MyProgramActivity::class.java))
        finish()

    }
}