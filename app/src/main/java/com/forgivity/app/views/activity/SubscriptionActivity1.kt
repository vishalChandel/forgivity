package com.forgivity.app.views.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R

class SubscriptionActivity1 : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription1)
        ButterKnife.bind(this)
    }
    @OnClick(
        R.id.btnContinueSubscription1
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.btnContinueSubscription1 -> performContinueClick()
        }
    }

    private fun performContinueClick() {

        startActivity(Intent(mActivity, SubscriptionActivity2::class.java))
        finish()
    }
}