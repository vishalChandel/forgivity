package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.adapter.GlossaryListAdapter
import com.forgivity.app.model.GlossaryListModel
import com.forgivity.app.viewModel.GlossaryListViewModel
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_glossary.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class GlossaryActivity : BaseActivity() {

    // - - Initialize Objects
    var glossaryListAdapter: GlossaryListAdapter? = null
    private var glossaryListViewModel: GlossaryListViewModel? = null
    var mGlossaryList: ArrayList<GlossaryListModel> = ArrayList<GlossaryListModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_glossary)
        ButterKnife.bind(this)
        glossaryListViewModel = ViewModelProviders.of(this).get(GlossaryListViewModel::class.java)
        executeGlossaryListApi()
    }

    @OnClick(
        R.id.drawerRL,
        R.id.txtGlossaryDetailTV

    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.drawerRL -> performDrawerClick()
            R.id.txtGlossaryDetailTV -> performGlossaryDetailClick()
        }
    }

    private fun performGlossaryDetailClick() {
        startActivity(Intent(mActivity, GlossaryDetailActivity::class.java))
    }

    private fun performDrawerClick() {
        startActivity(Intent(mActivity, HomeActivity::class.java))
        overridePendingTransition(
            R.anim.right_to_left,
            R.anim.left_to_right
        )
    }

    // - - Execute Glossary List Api
    private fun executeGlossaryListApi() {
        showProgressDialog(mActivity)
        glossaryListViewModel?.getGlossaryListData(mActivity, getAccessToken())?.observe(this,
            androidx.lifecycle.Observer<JsonObject?> { mModel ->
                dismissProgressDialog()
                try {
                    val jsonObjectMM = JSONObject(mModel.toString())
                    val jsonObject: JSONObject = jsonObjectMM.getJSONObject("all_data")
                    Log.e(TAG, "obj: " + jsonObjectMM.getJSONObject("all_data"))
                    val keys = jsonObject.keys()
                    while (keys.hasNext()) {
                        val key = keys.next()
                        Log.e(TAG, "setJsonModel: $key")
                        //                        txtDateTV.setText(key);
                        if (jsonObject[key] is JSONArray) {
                            for (i in 0 until (jsonObject[key] as JSONArray).length()) {
                                val mObject = (jsonObject[key] as JSONArray).getJSONObject(i)
                                val model = GlossaryListModel()
                                model.word = mObject.getString("word")
                                model.disable = mObject.getString("disable")
                                model.glossaryId = mObject.getString("glossary_id")
                                model.definition = mObject.getString("definition")
                                model.creationDate = mObject.getString("creation_date")
                                model.pageRefs = mObject.getString("page_refs")
                                mGlossaryList.add(model)
                            }
                        }
                    }
                } catch (e: Exception) {
                    Log.e(TAG, "setJsonModel: $e")
                }

                setGlossaryAdapter()
            })
    }

    private fun setGlossaryAdapter() {
        if (glossaryListRV != null) {
            glossaryListAdapter = GlossaryListAdapter(mActivity, mGlossaryList)
            glossaryListRV.adapter = glossaryListAdapter
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.right_to_left,R.anim.left_to_right)
    }
}