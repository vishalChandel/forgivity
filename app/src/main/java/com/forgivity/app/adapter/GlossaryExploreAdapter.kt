package com.forgivity.app.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.forgivity.app.R
import com.forgivity.app.interfaces.LoadMoreGlossaryData
import com.forgivity.app.model.GlossaryModel
import com.forgivity.app.model.ListItemGlossary
import com.forgivity.app.views.activity.GlossaryDetailActivity

class GlossaryExploreAdapter(
    var mActivity: Activity,
    var mArrayList: List<ListItemGlossary?>?,
    var mLoadMoreGlossaryData: LoadMoreGlossaryData?,
    var glossaryModel: GlossaryModel?
) : RecyclerView.Adapter<GlossaryExploreAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_explore, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mList: ListItemGlossary? = mArrayList!![position]
        mActivity?.let {
            Glide.with(it).load(glossaryModel?.allData?.typeFile)
                .placeholder(R.drawable.ic_app)
                .error(R.drawable.ic_app)
                .into(holder.imgExploreIV)
        }
        holder.txtExploreNameTV.text= mArrayList!![position]!!.word
        holder.txtExploreDetailTV.text= mArrayList!![position]!!.definition
        holder.itemView.setOnClickListener {
            val intent = Intent(mActivity, GlossaryDetailActivity::class.java)
            intent.putExtra("glossary_id", mList?.glossaryId)
            mActivity.startActivity(intent)
        }
        if (mArrayList!!.size % 10 == 0 && mArrayList!!.size - 1 == position) {
            if (mList != null) {
                mLoadMoreGlossaryData?.onLoadMoreGlossaryData(mList)
            }
        }
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgExploreIV: ImageView = itemView.findViewById(R.id.imgExploreIV)
        var imgPlayIV: ImageView = itemView.findViewById(R.id.imgPlayIV)
        var txtExploreNameTV: TextView = itemView.findViewById(R.id.txtExploreNameTV)
        var txtExploreDetailTV: TextView = itemView.findViewById(R.id.txtExploreDetailTV)
    }

}