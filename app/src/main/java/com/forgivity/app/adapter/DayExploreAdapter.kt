package com.forgivity.app.adapter

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.forgivity.app.R
import com.forgivity.app.interfaces.LoadMoreDaysData
import com.forgivity.app.model.DayModel
import com.forgivity.app.model.ListItemDay
import com.forgivity.app.views.activity.AudioActivity

class DayExploreAdapter(
    var mActivity: Activity,
    private var mArrayList: ArrayList<ListItemDay?>?,
    var mLoadMoreDaysData: LoadMoreDaysData? = null,
    var dayModel: DayModel?
) : RecyclerView.Adapter<DayExploreAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_explore, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mList: ListItemDay? = mArrayList!![position]
        mActivity?.let {
            Glide.with(it).load(dayModel?.allData?.typeFile)
                .placeholder(R.drawable.ic_app)
                .error(R.drawable.ic_app)
                .into(holder.imgExploreIV)
        }
        holder.txtExploreNameTV.text = mArrayList!![position]!!.title
        holder.txtExploreDetailTV.text = mArrayList!![position]!!.description

        holder.itemView.setOnClickListener {
            showNuggetAlertDialog(mActivity, position)
        }
        if (mArrayList!!.size % 10 == 0 && mArrayList!!.size - 1 == position) {
            if (mList != null) {
                mLoadMoreDaysData?.onLoadMoreDaysData(mList)
            }
        }
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgExploreIV: ImageView = itemView.findViewById(R.id.imgExploreIV)
        var imgPlayIV: ImageView = itemView.findViewById(R.id.imgPlayIV)
        var txtExploreNameTV: TextView = itemView.findViewById(R.id.txtExploreNameTV)
        var txtExploreDetailTV: TextView = itemView.findViewById(R.id.txtExploreDetailTV)
    }

    // - - Nugget Dialog
    private fun showNuggetAlertDialog(mActivity: Context?, position: Int) {
        val alertDialog = Dialog(mActivity!! as Context)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_podcast)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val imgCrossIV = alertDialog.findViewById<ImageView>(R.id.imgCrossIV)
        val txtTitleTV = alertDialog.findViewById<TextView>(R.id.txtTitleTV)
        val txtDescriptionTV = alertDialog.findViewById<TextView>(R.id.txtDescriptionTV)
        val txtRelatedListeningTV = alertDialog.findViewById<TextView>(R.id.txtRelatedListeningTV)
        txtTitleTV.text = mArrayList?.get(position)!!.title
        txtDescriptionTV.text = mArrayList?.get(position)!!.description
        when (mArrayList?.get(position)!!.relatedPod) {
            "" -> {
                txtRelatedListeningTV.visibility = View.GONE
            }
            else -> txtRelatedListeningTV.visibility = View.VISIBLE
        }
        imgCrossIV.setOnClickListener {
            alertDialog.dismiss()
        }
        txtRelatedListeningTV.setOnClickListener {
            alertDialog.dismiss()
            val intent = Intent(mActivity, AudioActivity::class.java)
            intent.putExtra("audioFile", mArrayList?.get(position)?.relatedPod)
            intent.putExtra("audioFileName", mArrayList?.get(position)?.title)
            mActivity.startActivity(intent)
        }
        alertDialog.show()
    }
}