package com.forgivity.app.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.forgivity.app.R
import com.forgivity.app.model.GlossaryListModel
import com.forgivity.app.views.activity.GlossaryDetailActivity
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter
import java.util.*

/**
 * Created by android-da on 6/7/18.
 */
class GlossaryListAdapter(
    private val context: Context,
    var mChatgroupList: ArrayList<GlossaryListModel>,
    var newConvertView: View? = null,
    var headerConvertView: View? = null
) :
    BaseAdapter(), StickyListHeadersAdapter {
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int {
        return mChatgroupList.size
    }

    override fun getItem(position: Int): Any {
        return mChatgroupList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        newConvertView = convertView
        val mModel: GlossaryListModel = mChatgroupList[position]
        val holder: ViewHolder
        if (newConvertView == null) {
            holder = ViewHolder()
            newConvertView = inflater.inflate(R.layout.item_glossary_list, parent, false)
            holder.txtWordTV = newConvertView!!.findViewById<TextView>(R.id.txtWordTV)
            newConvertView!!.tag = holder
        } else {
            holder = newConvertView!!.tag as ViewHolder
        }
        try {
            holder.txtWordTV!!.text = mModel.word
            holder.txtWordTV!!.setOnClickListener {
                val intent = Intent(context, GlossaryDetailActivity::class.java)
                intent.putExtra("glossary_id", mModel.glossaryId)
                context.startActivity(intent)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return newConvertView!!
    }

    override fun getHeaderView(position: Int, convertView: View?, parent: ViewGroup): View {
        headerConvertView = convertView
        val holder: HeaderViewHolder
        if (headerConvertView == null) {
            holder = HeaderViewHolder()
            headerConvertView =
                inflater.inflate(R.layout.item_glossary_header_layout, parent, false)
            holder.headerTV = headerConvertView!!.findViewById(R.id.headerTV)
            headerConvertView!!.tag = holder
        } else {
            holder = headerConvertView?.tag as HeaderViewHolder
        }

        //set header text as first char in name
        if (mChatgroupList[position].word != null && mChatgroupList[position].word!!.isNotEmpty()
        ) {
            val headerText = "" + mChatgroupList[position].word!!.get(0)
            holder.headerTV!!.text = headerText
        }
        return headerConvertView!!
    }

    override fun getHeaderId(position: Int): Long {
        //return the first character of the country as ID because this is what headers are based upon
        if (mChatgroupList.size > 0)
            return mChatgroupList[position].word?.get(0)?.toLong()!!
        else
            return 0
    }


    internal inner class HeaderViewHolder {
        var headerTV: TextView? = null
    }

    class ViewHolder {
        var txtWordTV: TextView? = null
    }

}

