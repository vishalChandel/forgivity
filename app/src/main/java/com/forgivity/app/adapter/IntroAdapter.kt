package com.forgivity.app.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.forgivity.app.R
import com.forgivity.app.views.activity.NameActivity
import com.forgivity.app.views.activity.StartActivity


class IntroAdapter(private val mActivity: Activity) : PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {

        return view === `object`
    }

    override fun getCount(): Int {
        return 8
    }

    override fun instantiateItem(mViewGroup: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(mActivity)
        var layout: ViewGroup? = null

        when (position) {
            0 -> {
                layout =
                    inflater.inflate(R.layout.item_intro1, mViewGroup, false) as ViewGroup?
                mViewGroup.addView(layout)
            }
            1 -> {
                layout =
                    inflater.inflate(R.layout.item_intro2, mViewGroup, false) as ViewGroup?
                mViewGroup.addView(layout)
            }
            2 -> {
                layout =
                    inflater.inflate(R.layout.item_intro3, mViewGroup, false) as ViewGroup?
                mViewGroup.addView(layout)
            }
            3 -> {
                layout =
                    inflater.inflate(R.layout.item_intro4, mViewGroup, false) as ViewGroup?
                mViewGroup.addView(layout)
            }
            4 -> {
                layout =
                    inflater.inflate(R.layout.item_intro5, mViewGroup, false) as ViewGroup?
                mViewGroup.addView(layout)
            }
            5 -> {
                layout =
                    inflater.inflate(R.layout.item_intro6, mViewGroup, false) as ViewGroup?
                mViewGroup.addView(layout)
            }
            6 -> {
                layout =
                    inflater.inflate(R.layout.item_intro7, mViewGroup, false) as ViewGroup?
                mViewGroup.addView(layout)
            }
            7 -> {
                layout =
                    inflater.inflate(R.layout.item_intro8, mViewGroup, false) as ViewGroup?
                mViewGroup.addView(layout)
                val imgNextIV: ImageView = layout?.findViewById<View>(R.id.imgNextIV) as ImageView
                imgNextIV.setOnClickListener {
                    val intent = Intent(mActivity, StartActivity::class.java)
                    mActivity.startActivity(intent)
                    mActivity.finish()
                }
            }
        }
        return layout!!
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }
}


