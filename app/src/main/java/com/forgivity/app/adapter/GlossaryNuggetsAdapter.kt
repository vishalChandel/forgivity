package com.forgivity.app.adapter

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.forgivity.app.R
import com.forgivity.app.model.GlossaryDetailModel

class GlossaryNuggetsAdapter(var Context: Context?, var mModel: GlossaryDetailModel) :
    RecyclerView.Adapter<GlossaryNuggetsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_nuggets_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.txtNuggetTitleTV.text = mModel.savedDetail?.linkedPages?.get(position)!!.title
        holder.txtNuggetDetailTV.text = mModel.savedDetail?.linkedPages?.get(position)!!.description
        holder.itemView.setOnClickListener {
            showNuggetAlertDialog(Context, position)
        }
    }

    override fun getItemCount(): Int {
        return mModel.savedDetail!!.linkedPages!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtNuggetTitleTV: TextView = itemView.findViewById(R.id.txtNuggetTitleTV)
        var txtNuggetDetailTV: TextView = itemView.findViewById(R.id.txtNuggetDetailTV)
    }

    // - - Nugget Dialog
    private fun showNuggetAlertDialog(mActivity: Any?, position: Int) {
        val alertDialog = Dialog(mActivity!! as Context)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_nugget)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val imgCrossIV = alertDialog.findViewById<ImageView>(R.id.imgCrossIV)
        val nuggetTitleTV = alertDialog.findViewById<TextView>(R.id.nuggetTitleTV)
        val nuggetsDetailTV = alertDialog.findViewById<TextView>(R.id.nuggetsDetailTV)
        nuggetTitleTV.text = mModel.savedDetail?.linkedPages?.get(position)!!.title
        nuggetsDetailTV.text = mModel.savedDetail?.linkedPages?.get(position)!!.description
        imgCrossIV.setOnClickListener {
            alertDialog.dismiss()
        }
        alertDialog.show()
    }
}