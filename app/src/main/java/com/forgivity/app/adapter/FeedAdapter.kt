package com.forgivity.app.adapter

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.forgivity.app.R
import com.forgivity.app.interfaces.LoadMoreActivityFeedData
import com.forgivity.app.model.ListItemFeed
import com.forgivity.app.views.activity.AudioActivity

class FeedAdapter(
    var mActivity: Activity, var mArrayList: ArrayList<ListItemFeed?>?,
    var mLoadMoreScrollListener: LoadMoreActivityFeedData?=null) :
    RecyclerView.Adapter<FeedAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_activity_feed, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mModel: ListItemFeed? = mArrayList!![position]
        holder.notificationTitleTV.text = mArrayList?.get(position)?.notifyType
        holder.notificationDetailTV.text = mArrayList?.get(position)?.notifyMessage
        if (mArrayList!!.size % 10 == 0 && mArrayList!!.size - 1 == position) {
            if (mModel != null) {
                mLoadMoreScrollListener?.onLoadMoreActivityFeedData(mModel)
            }
        }

        holder.itemView.setOnClickListener {
            when (mArrayList?.get(position)?.notifyType) {
                "nugget" -> {
                    showPodcastAlertDialog(mActivity)
                }
                "podcast" -> {
                    val intent = Intent(mActivity, AudioActivity::class.java)
                    mActivity.startActivity(intent)
                }
                "survey" -> {
                    showSurveyResultAlertDialog(mActivity)
                }
                "mass" -> {

                }
            }
        }
    }

    override fun getItemCount(): Int {
        return mArrayList?.size!!
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var notificationTitleTV: TextView = itemView.findViewById(R.id.notificationTitleTV)
        var notificationDetailTV: TextView = itemView.findViewById(R.id.notificationDetailTV)
    }

    // - - Podcast Alert Dialog
    private fun showPodcastAlertDialog(mActivity: Any?) {
        val alertDialog = Dialog(mActivity!! as Context)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_podcast)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val imgCrossIV = alertDialog.findViewById<ImageView>(R.id.imgCrossIV)
        imgCrossIV.setOnClickListener {
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    // - - Survey Result Alert Dialog
    private fun showSurveyResultAlertDialog(mActivity: Any?) {
        val alertDialog = Dialog(mActivity!! as Context)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_survey_three_result)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val btnDone = alertDialog.findViewById<TextView>(R.id.btnDone)
        btnDone.setOnClickListener {
            alertDialog.dismiss()
        }
        alertDialog.show()
    }
}