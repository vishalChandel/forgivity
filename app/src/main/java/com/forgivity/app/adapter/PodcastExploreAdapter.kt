package com.forgivity.app.adapter

import android.app.Activity
import android.content.Intent
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.forgivity.app.R
import com.forgivity.app.interfaces.LoadMorePodcastData
import com.forgivity.app.model.ListItemPodcast
import com.forgivity.app.model.PodcastModel
import com.forgivity.app.views.activity.AudioActivity

class PodcastExploreAdapter(
    var mActivity: Activity,
    var mArrayList: ArrayList<ListItemPodcast?>?,
    var mLoadMorePodcastData: LoadMorePodcastData?,
    var podcastModel: PodcastModel?
) : RecyclerView.Adapter<PodcastExploreAdapter.ViewHolder>() {
    var mTabClick = 0L

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_explore, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val mList: ListItemPodcast? = mArrayList!![position]
        mActivity?.let {
            Glide.with(it).load(podcastModel?.allData?.typeFile)
                .placeholder(R.drawable.ic_app)
                .error(R.drawable.ic_app)
                .into(holder.imgExploreIV)
        }
        holder.txtExploreNameTV.text = mArrayList!![position]!!.podTitle
        holder.txtExploreDetailTV.text = mArrayList!![position]!!.podDesc
        holder.itemView.setOnClickListener {
            // Preventing multiple clicks, using threshold of 1 second
            if (SystemClock.elapsedRealtime() - mTabClick < 1500) {
                return@setOnClickListener
            }
            mTabClick = SystemClock.elapsedRealtime()
            val intent = Intent(mActivity, AudioActivity::class.java)
            intent.putExtra("audioFile", mList?.podFile)
            intent.putExtra("audioFileName", mList?.podTitle)
            mActivity.startActivity(intent)
        }
        if (mArrayList!!.size % 10 == 0 && mArrayList!!.size - 1 == position) {
            if (mList != null) {
                mLoadMorePodcastData?.onLoadMorePodcastData(mList)
            }
        }
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgExploreIV: ImageView = itemView.findViewById(R.id.imgExploreIV)
        var imgPlayIV: ImageView = itemView.findViewById(R.id.imgPlayIV)
        var txtExploreNameTV: TextView = itemView.findViewById(R.id.txtExploreNameTV)
        var txtExploreDetailTV: TextView = itemView.findViewById(R.id.txtExploreDetailTV)
    }
}