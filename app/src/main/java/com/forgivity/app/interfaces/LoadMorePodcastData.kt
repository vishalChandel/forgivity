package com.forgivity.app.interfaces

import com.forgivity.app.model.ListItemPodcast

interface LoadMorePodcastData {
    public fun onLoadMorePodcastData(mModel : ListItemPodcast)
}