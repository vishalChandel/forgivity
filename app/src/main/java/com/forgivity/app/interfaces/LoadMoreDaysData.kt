package com.forgivity.app.interfaces

import com.forgivity.app.model.ListItemDay

interface LoadMoreDaysData {
    public fun onLoadMoreDaysData(mModel : ListItemDay)
}