package com.forgivity.app.interfaces

import com.forgivity.app.model.ListItemFeed

interface LoadMoreActivityFeedData {
    public fun onLoadMoreActivityFeedData(mModel: ListItemFeed)
}