package com.forgivity.app.interfaces

import com.forgivity.app.model.ListItem

interface LoadMoreSavedData {
    public fun onLoadMoreSavedData(mModel: ListItem)
}