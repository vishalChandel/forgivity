package com.forgivity.app.interfaces

import com.forgivity.app.model.ListItemGlossary

interface LoadMoreGlossaryData {
    public fun onLoadMoreGlossaryData(mModel : ListItemGlossary)
}